# OffOn.io API Rest

## Description

API REST para la aplicación OffOn.io - App para códigos de publicidad, cuando se introduce un código se muestra el contenido asociado.

## Installation

Clone the repository and run the following commands under your project root:

```shell
npm install
npm start
```