'use strict';

const configImage = require('../../config/services/image');

/**
 * UserController
 * @description :: Server-side logic for manage users
 */

module.exports = {
  full(req, res) {
    const id = req.param('id');
    if (_.isUndefined(id)) {
      User
        .find()
        .populate('person')
        .populate('roles')
        .then(res.ok)
        .catch(res.negotiate);
    } else {
      User
        .findOne({ id })
        .populate('person')
        .populate('roles')
        .then(res.ok)
        .catch(res.negotiate);
    }
  },

  me(req, res) {
    const id = req.user.id;
    // GET read
    if(req.method === 'GET'){
      User
        .findOne({ id })
        .populate('person')
        .populate('roles')
        .then(res.ok)
        .catch(res.negotiate);
    } 
    
    // PUT update
    if(req.method === 'PUT'){
       User.update({ id }, req.body)
      .then(res.ok)
      .catch(res.negotiate);
    }
  },

  closeMe(req, res) {
    const id = req.user.id;
    if(req.method === 'POST'){
      User
      .findOne({ id })
      .then(user => {
        if (!HashService.bcrypt.compareSync(req.body.password, user.password)) return res.negotiate(sails.config.errors.USER_NOT_FOUND);
        user.active = false;
        user.save(error => {
          if (error) return res.negotiate(error);
          return res.ok();
        });
      }).catch(res.negotiate);
    }else{
      return res.notFound();
    }
  },

  changePasswordMe(req, res) {
    const id = req.user.id;
    if(req.method === 'POST'){
      User
      .findOne({ id })
      .then(user => {
        if (!HashService.bcrypt.compareSync(req.body.currentPassword, user.password)) return res.negotiate(sails.config.errors.USER_NOT_FOUND);
        if (!req.body.newPassword) return res.negotiate(sails.config.errors.BAD_REQUEST);
          user.password = req.body.newPassword;
          user.save(error => {
            if (error) return res.negotiate(error);
            return res.ok();
          });
      }).catch(res.negotiate);
    }else{
      return res.notFound();
    }
  },

  avatar(req, res){
    const id = req.user.id,
          uploadDir = require('path').resolve(sails.config.appPath, `assets${configImage.folders.avatar}`);
    req.file('avatar').upload({
      dirname: uploadDir
    }, (error, files) => {
      if (error) return res.negotiate(error);
      
      // If no files were uploaded, respond with an error.
      if (files.length === 0) return res.badRequest('No file was uploaded');

      let nameFile =  files[0].fd.split('/');
      nameFile = nameFile[(nameFile.length - 1)];

      // Save the "fd" and the url where the avatar for a user can be accessed
      User.update({ id }, {
        // Generate a unique URL where the avatar can be downloaded.
        photo: require('util').format('%s' + configImage.folders.avatar + '/%s', Config.getUrl(), nameFile)
      })
      .then(users => {
        return res.ok({ photo: _.head(users).photo });
      })
      .catch(res.negotiate);
    });
  },
};
