'use strict';

module.exports = {
    /**
     * getUrl
     * @description :: Method to obtain the URL of our system
     */
    getUrl(){
        const usingSSL = (sails.config.environment === 'production'),
              port = sails.config.proxyPort || sails.config.port;
        return (usingSSL ? 'https' : 'http') + '://' +
        (sails.config.hostApp || 'localhost') +
        (port == 80 || port == 443 || usingSSL ? '' : ':' + port);
    }
}