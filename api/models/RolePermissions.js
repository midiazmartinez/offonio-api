'use strict'

/**
 * RolePermissions
 * @description :: Model for storing RolePermissions records
 */

module.exports = {
  schema: true,

  tableName: 'roles_permissions',

  attributes: {
    roles: {
      model: 'role'
    },

    permissions: {
      model: 'permission'
    },

    toJSON() {
      return this.toObject()
    }
  },

  beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()

  /*seedData: [
    // Role 'super'
    {'roles': 1,'permissions': 1}, // create
    {'roles': 1,'permissions': 2}, // read
    {'roles': 1,'permissions': 3}, // update
    {'roles': 1,'permissions': 4}, // delete

    // Role 'administrator'
    {'roles': 2,'permissions': 1}, // create
    {'roles': 2,'permissions': 2}, // read
    {'roles': 2,'permissions': 3}, // update
    {'roles': 2,'permissions': 4}, // delete
    
    // Role 'guest'
    {'roles': 3,'permissions': 1}, // create
    {'roles': 3,'permissions': 2}, // read
    {'roles': 3,'permissions': 3}, // update
    {'roles': 3,'permissions': 4}, // delete
  ]*/
}
