'use strict';

/**
 * User
 * @description :: Model for storing users
 */

const _ = require('lodash');

module.exports = {
  schema: true,

  attributes: {
    username: {
      type: 'string',
      required: true,
      unique: true,
      alphanumericdashed: true
    },

    password: {
      type: 'string',
      required: true,
      minLength: 8
    },

    email: {
      type: 'email',
      required: true,
      unique: true
    },

    photo: {
      type: 'string',
      defaultsTo: '',
      url: true
    },

    socialProfiles: {
      type: 'json',
      defaultsTo: {}
    },

    active: {
      type: 'boolean',
      defaultsTo: false
    },

    person: {
      model: 'person'
    },

    roles: {
      collection: 'role',
      via: 'users',
      through: 'userroles'
    },

    toJSON() {
      let obj = this.toObject();
      delete obj.password;
      delete obj.socialProfiles;
      return obj;
    },
  },

  beforeUpdate(values, next) {
    if (false === values.hasOwnProperty('password')) return next();
    if (/^\$2[aby]\$[0-9]{2}\$.{53}$/.test(values.password)) return next();

    return HashService.bcrypt.hash(values.password)
      .then(hash => {
        values.password = hash;
        next();
      })
      .catch(next);
  },

  beforeCreate(values, next) {
    if (false === values.hasOwnProperty('password')) return next();
    return HashService.bcrypt.hash(values.password)
      .then(hash => {
        values.password = hash;
        next();
      })
      .catch(next);
  },

  afterCreate: [
    (user, next) => {
      if (user.person) {
        Person.update({
          id: user.person
        }, {
          user: user.id
        }).exec((error, persons) => {
          if (error) {
            return next(error);
          }
          user.person = _.head(persons);
          return next();
        });
      }
    },
    (user, next) => {
      User.findOne({
          id: user.id
        })
        .then(_user => {
          user = _user;
          return Role.findOne({
            name: 'administrator'
          });
        })
        .then(role => {
          UserRoles.create({
            users: user.id,
            roles: role.id
          }).exec((error, userroles) => {
            if (error) {
              return next(error);
            }
              return next();
            });
        })
        .catch(function (e) {
          next(e);
        })
    }
  ]
};