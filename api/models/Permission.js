'use strict';

/**
 * Permission
 * @description :: Model for storing Permission records
 */

module.exports = {
  schema: true,

  attributes: {

    action: {
      type: 'string',
      index: true,
      notNull: true,
      enum: [
        'create',
        'read',
        'update',
        'delete'
      ],
      unique: true,
      defaultsTo: 'read'
    },

    roles: {
      collection: 'role',
      via: 'permissions',
      through: 'rolepermissions'
    },

    toJSON() {
      return this.toObject();
    }
  },

  beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()

  /*seedData: [
    {'action': 'create'},
    {'action': 'read'},
    {'action': 'update'},
    {'action': 'delete'}
  ]*/
};
