"use strict";

/**
 * ContentTypes
 * @description :: Model for storing ContentTypes records
 */

module.exports = {
  schema: true,

  description: 'Contains the content types available in offon.io',

  attributes: {
    name: {
      type: 'string',
      notNull: true,
      unique: true
    },

    code: {
      type: 'string',
      required: true,
      unique: true,
      alphanumericdashed: true
    },

    active: {
      type: 'boolean',
      defaultsTo: true
    },

    description: {
      type: 'string'
    },

    toJSON() {
      return this.toObject();
    }
  },

  beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()
};
