'use strict';

/**
 * Default model configuration
 *
 * Unless you override them, the following properties will be included in each of your models.
 */

module.exports = {
  models: {
    /**
     * Your app's default connection
     * @type {String}
     */
    connection: 'mysql',

    /**
     * How and whether Sails will attempt to automatically rebuild the tables/collections/etc. in your schema
     * Available values is `safe`, `alter` or `drop`
     * @type {String}
     */
    migrate: 'alter',

    /**
     * This method adds records to the database
     *
     * To use add a variable 'seedData' in your model and call the
     * method in the bootstrap.js file
     */
    seed (callback) {
      const modelName = this.adapter.identity.charAt(0).toUpperCase() + this.adapter.identity.slice(1);
      if (!this.seedData) {
        sails.log.debug(`No data avaliable to seed ${modelName}`);
        callback();
        return;
      }
      this.count().exec((err, count) => {
        if (!err && count === 0) {
          sails.log.debug(`Seeding ${modelName}...`);
          if (this.seedData instanceof Array) {
            this.seedArray(callback, modelName);
          } else {
            this.seedObject(callback, modelName);
          }
        } else {
          sails.log.debug(`${modelName} had models, so no seed needed`);
          callback();
        }
      });
    },

    seedArray (callback, modelName) {
      this.createEach(this.seedData).exec((err, results) => {
          sails.log.debug(err || `${modelName} seed planted`);
          callback();
      });
    },

    seedObject (callback, modelName) {
      this.create(this.seedData).exec((err, results) => {
          sails.log.debug(err || `${modelName} seed planted`);
          callback();
      });
    }
  }
};
