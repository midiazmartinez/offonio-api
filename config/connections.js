'use strict';

/**
 * Connections API Configuration
 *
 * Connections are like "saved settings" for your adapters.
 * Each model must have a `connection` property (a string) which is references the name of one
 * of these connections.  If it doesn't, the default `connection` configured in `config/models.js`
 * will be applied.  Of course, a connection can (and usually is) shared by multiple models.
 *
 * NOTE: If you're using version control, you should put your passwords/api keys
 * in `config/local.js`, environment variables, or use another strategy.
 */

module.exports = {
  connections: {
    /**
     * MySQL configuration
     * @type {Object}
     */
    mysql: {
      adapter: 'sails-mysql',
      host: 'localhost',
      port: 3306,
      user: 'root',
      password: 'QAZwsx123.',
      database: 'offonio'
    },

    /**
     * MySQL configuration PROD
     * @type {Object}
     */
    mysql_prod: {
      adapter: 'sails-mysql',
      host: 'eu-cdbr-azure-west-b.cloudapp.net',
      port: 3306,
      user: 'b09db3fdfc93ea',
      password: '3d7b13d6',
      database: 'offonio'
    },

    /**
     * MySQL configuration PROD Azure
     * @type {Object}
     */
    mysql_prod_azure: {
      adapter: 'sails-mysql',
      host: 'offonio.mysql.database.azure.com',
      port: 3306,
      user: 'super@offonio',
      password: 'QAZwsx123.',
      database: 'offonio'
    }
  }
};
