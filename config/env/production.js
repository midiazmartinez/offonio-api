'use strict';

/**
 * Production environment settings
 * @description :: This section overrides all other config values ONLY in production environment
 */

module.exports = {
  port: 80,
  hostApp: 'api.offon.io',
  log: {
    level: 'info'
  },
  models: {
    connection: 'mysql_prod'
  },
  cors: {
    origin: 'https://offon.io,https://www.offon.io'
  }
};
