'use strict';

module.exports = {
  swagger: {
    pkg: {
      name: 'OffOn.io API',
      description: 'API REST para la app [`OffOn.io`](http://offon.io/) un lector de códigos de publicidad donde se introduce un código y se muestra el contenido asociado a ese código.',
      basePath: '/v1',
      version: '0.1.0',
      author: 'OffOn.io',
      homepage: 'About',
      license: ''
    }
  }
};
